'''
Created on 7 nov. 2020

@author: RSSpe
'''

from auto import Auto
from ordenamiento_intercalacion import OrdenamientoIntercalacion
import pickle


def llenar1(ruta1:str) -> None:

    archivo : File = open(ruta1, "wb")

    pickle.dump(Auto("Supra", "Toyota", 1998), archivo)
    pickle.dump(Auto("Rx7", "Mazda", 2002), archivo)

    archivo.close()

    del (archivo)


def llenar2(ruta2:str) -> None:

    archivo : File = open(ruta2, "wb")

    pickle.dump(Auto("Dodge", "Charger", 1997), archivo)
    pickle.dump(Auto("Eclipse", "Mitsubishi", 1999), archivo)
    pickle.dump(Auto("Dodge", "challenger", 2006), archivo)
    pickle.dump(Auto("Lancer evo", "Mitsubishi", 2007), archivo)

    archivo.close()

    del (archivo)


def main() -> None:

    a1 : list = [1, 3, 5, 7, 9]
    a2 : list = [2, 4, 6, 8, 10]

    print(OrdenamientoIntercalacion.array(a1, a2))

    print("\n-----------------------------------------------------")

    ruta1 = "./archivos/autos1.bin"
    ruta2 = "./archivos/autos2.bin"
    ruta3 = "./archivos/db.bin"

    llenar1(ruta1)
    llenar2(ruta2)

    #print(f"\n{OrdenamientoIntercalacion.archivoInterativo(ruta1, ruta2, ruta3)}\n")
    print(f"\n{OrdenamientoIntercalacion.archivoRecursivo(ruta1, ruta2, ruta3)}\n")
    print("------------------Autos archivo 1---------------------")
    OrdenamientoIntercalacion.imprimir(ruta1)
    print("\n------------------Autos archivo 2---------------------")
    OrdenamientoIntercalacion.imprimir(ruta2)
    print("\n-------------Autos en la base de datos---------------")
    OrdenamientoIntercalacion.imprimir(ruta3)


if __name__ == '__main__':
    main()
