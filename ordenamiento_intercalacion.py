
import pickle
import os


class OrdenamientoIntercalacion:

    @staticmethod
    def array(a1:list, a2:list) -> list:
        
        a3 : list = []
        cont : int = 0
        cont2 : int = 0

        while len(a1)!=cont and len(a2)!=cont2:

            if a1[cont]<=a2[cont2]:
                a3.append(a1[cont])
                cont+=1
            else:
                a3.append(a2[cont2])
                cont2+=1

        while len(a1)!=cont:
            a3.append(a1[cont])
            cont+=1

        while len(a2)!=cont2:
            a3.append(a2[cont2])
            cont2+=1

        return a3


    @staticmethod
    def archivoInterativo(ruta1:str, ruta2:str,
                          ruta3:str) -> str:

        archivo1 : File = open(ruta1, "r")
        archivo2 : File = open(ruta2, "r")


        if os.stat(ruta1).st_size == 0 and os.stat(ruta2).st_size==0:
            return f"Sin elementos"

        elif os.stat(ruta1).st_size == 0 or os.stat(ruta2).st_size==0:
            return f"Ningun archivo debe de estar vacio"


        try:
            archivo1 : File = open(ruta1, "rb")
            archivo2 : File = open(ruta2, "rb")
            archivo3 : File = open(ruta3, "wb")
            errorArchivo : int = 0
        
            obj = pickle.load(archivo1)
            obj2 = pickle.load(archivo2)
            año = obj.año
            año2 = obj2.año

            while errorArchivo==0:

                if año<=año2:
                    try:
                        pickle.dump(obj, archivo3)
                        obj = pickle.load(archivo1)
                        año = obj.año
                    except EOFError:
                        errorArchivo = 1
                else:
                    try:
                        pickle.dump(obj2, archivo3)
                        obj2 = pickle.load(archivo2)
                        año2 = obj2.año
                    except EOFError:
                        errorArchivo = 2

            while True:

                try:
                    if errorArchivo==1:
                        pickle.dump(obj2, archivo3)
                        obj2 = pickle.load(archivo2)
                        año2 = obj2.año

                    else:
                        pickle.dump(obj, archivo3)
                        obj = pickle.load(archivo1)
                        año = obj.año

                except EOFError:
                    break

            archivo1.close()
            archivo2.close()
            archivo3.close()

            del (archivo1)
            del (archivo2)
            del (archivo3)

            return "La base de datos se ha cargado    :)"

        except FileNotFoundError:
            return "Error no se pudo localizar algun archivo"


    @staticmethod
    def archivoRecursivo(ruta1:str, ruta2:str, ruta3:str) -> str:

        archivo1 : File = open(ruta1, "r")
        archivo2 : File = open(ruta2, "r")


        if os.stat(ruta1).st_size == 0 and os.stat(ruta2).st_size==0:
            return f"Sin elementos"

        elif os.stat(ruta1).st_size == 0 or os.stat(ruta2).st_size==0:
            return f"Ningun archivo debe de estar vacio"


        try:
            archivo1 : File = open(ruta1, "rb")
            archivo2 : File = open(ruta2, "rb")
            archivo3 : File = open(ruta3, "wb")
            errorArchivo : int = 0
        
            obj = pickle.load(archivo1)
            obj2 = pickle.load(archivo2)
            año = obj.año
            año2 = obj2.año

            OrdenamientoIntercalacion.agregar(obj, obj2, año, año2, errorArchivo,
                    archivo1, archivo2, archivo3)

            archivo1.close()
            archivo2.close()
            archivo3.close()

            del (archivo1)
            del (archivo2)
            del (archivo3)

            return "La base de datos se ha cargado    :)"

        except FileNotFoundError:
            return "Error no se pudo localizar algun archivo"


    @staticmethod
    def agregar(obj, obj2, año, año2, errorArchivo,
            archivo1, archivo2, archivo3):

        if errorArchivo==0:

            if año<=año2:
                try:
                    pickle.dump(obj, archivo3)
                    obj = pickle.load(archivo1)

                    OrdenamientoIntercalacion.agregar(obj, obj2, obj.año, año2, errorArchivo,
                                                      archivo1, archivo2, archivo3)
                except EOFError:
                    OrdenamientoIntercalacion.agregar(obj, obj2, año, año2, 1,
                                                      archivo1, archivo2, archivo3)
            else:
                try:
                    pickle.dump(obj2, archivo3)
                    obj2 = pickle.load(archivo2)

                    OrdenamientoIntercalacion.agregar(obj, obj2, año, obj2.año, errorArchivo,
                                                      archivo1, archivo2, archivo3)
                except EOFError:
                    OrdenamientoIntercalacion.agregar(obj, obj2, año, año2, 2,
                                                      archivo1, archivo2, archivo3)

        else:
            try:
                if errorArchivo==1:
                    pickle.dump(obj2, archivo3)
                    obj2 = pickle.load(archivo2)

                    OrdenamientoIntercalacion.agregar(obj, obj2, año, obj2.año, errorArchivo,
                                                      archivo1, archivo2, archivo3)

                else:
                    pickle.dump(obj, archivo3)
                    obj = pickle.load(archivo1)

                    OrdenamientoIntercalacion.agregar(obj, obj2, obj.año, año2, errorArchivo,
                                                      archivo1, archivo2, archivo3)

            except EOFError:
                pass


    @staticmethod 
    def imprimir(ruta3:str):
        archivo3 : File = open(ruta3, "rb")

        while True:
            try:
                print(pickle.load(archivo3))
            except EOFError:
                break
