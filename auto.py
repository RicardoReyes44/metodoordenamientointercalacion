'''
Created on 7 nov. 2020

@author: RSSpe
'''


class Auto:
    
    def __init__(self, nombre, marca, año):
        self.__nombre = nombre
        self.__marca = marca
        self.__año = año

    @property
    def año(self):
        return self.__año

    def __str__(self):
        return f"Auto[nombre={self.__nombre}, marca={self.__marca}, año={self.__año}]"
